const request = require('supertest');
const app = require('../src/main.js');

describe('API Integration Tests', () => {
    it('responds to valid hex value', done => {
        request(app)
            .post('/api/hex-to-rgb')
            .send({ hex: '#FFFFFF' })
            .expect('Content-Type', /json/)
            .expect(200, {
                r: 255,
                g: 255,
                b: 255
            }, done);
    });

    it('responds with an error for invalid hex value', done => {
        request(app)
            .post('/api/hex-to-rgb')
            .send({ hex: '#GGG' })
            .expect('Content-Type', /json/)
            .expect(400, done);
    });

    // Add more tests for different scenarios
});
