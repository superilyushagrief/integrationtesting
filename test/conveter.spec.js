const expect = require('chai').expect;
const hexToRgb = require('../src/converter');

describe('Hex to RGB Conversion', () => {
    it('should convert basic hex to RGB', () => {
        const result = hexToRgb('#ffffff');
        expect(result).to.deep.equal({r: 255, g: 255, b: 255});
    });

    it('should convert red hex to RGB', () => {
        const result = hexToRgb('#ff0000');
        expect(result).to.deep.equal({r: 255, g: 0, b: 0});
    });

    it('should convert green hex to RGB', () => {
        const result = hexToRgb('#00ff00');
        expect(result).to.deep.equal({r: 0, g: 255, b: 0});
    });

    it('should convert blue hex to RGB', () => {
        const result = hexToRgb('#0000ff');
        expect(result).to.deep.equal({r: 0, g: 0, b: 255});
    });

    it('should handle hex without hash symbol', () => {
        const result = hexToRgb('ffffff');
        expect(result).to.deep.equal({r: 255, g: 255, b: 255});
    });

    it('should throw an error for invalid hex', () => {
        expect(() => hexToRgb('zzzzzz')).to.throw('Invalid Hex format');
    });

    it('should throw an error for empty string', () => {
        expect(() => hexToRgb('')).to.throw('Invalid Hex format');
    });

    it('should convert #000000 to RGB', () => {
        expect(hexToRgb('#000000')).to.deep.equal({r: 0, g: 0, b: 0});
    });

    it('should convert #123456 to RGB', () => {
        expect(hexToRgb('#123456')).to.deep.equal({r: 18, g: 52, b: 86});
    });

    it('should convert 3-digit hex code #FFF to RGB', () => {
        expect(hexToRgb('#FFF')).to.deep.equal({r: 255, g: 255, b: 255});
    });

    it('throws an error for hex code with invalid characters', () => {
        expect(() => hexToRgb('#GGG')).to.throw('Invalid Hex format');
    });

    it('throws an error for empty string', () => {
        expect(() => hexToRgb('')).to.throw('Invalid Hex format');
    });

    it('throws an error for null input', () => {
        expect(() => hexToRgb(null)).to.throw('Invalid Hex format');
    });
});

